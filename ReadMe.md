# Zabbix-Couchbase-5.x
Zabbix monitor template for Couchbase 5.x (Also support 3)

Couchbase Monitoring Template for Zabbix
====================

This template is a improved version of the template by [lamjonah](https://github.com/lamjonah/Zabbix-Couchbase-3.0-template)

**Files:**

    - userparameter_couchbase_3.0.conf (Configuration file for Zabbix Agent)
    - zbx_couchbase_templates_3.0.xml (Template that utilize the performance counter defined)

**Requirements:**

	- jq should be installed on the couchbase hosts.
	
	- zabbix user needs to be run /opt/couchbase/bin/couchbase-cli as root via sudoers. i.e:
	  zabbix ALL=(root) NOPASSWD: /opt/couchbase/bin/couchbase-cli

	- And macros listed below needed to be defined as host macros.
	    Macros:
		- {$USERNAME} -> Admin account username
		- {$PASSWORD} -> Admin account passwrod
		- {$BUCKET} -> Bucket Name
    
